---
date: 2022-08-05
filename: Readme.md
modification: 2022-08-05
author: Néhémie Strupler
---

An archive to support the reproducibility of the publication:\
*Strupler, 'Vandalising' Father Hittite. Karabel, Orientalism and Historiographies.*

This archive allows to reproduce the situation map (figure 1)
and provides the necessary script to reproduce the computational
environment, download the data, and re-produce the map.


## Data

This script downloads data from:

 - NASA JPL (2013). NASA Shuttle Radar Topography Mission Global
   1 arc second [Data set]. NASA EOSDIS Land Processes DAAC.
   https://doi.org/10.5067/MEaSUREs/SRTM/SRTMGL1.003

 - OpenStreetMap contributors (2022),
   OpenStreetMap [Data set], <https://www.openstreetmap.org>,
   distributed under the [Open Data Commons Open Database
License](https://opendatacommons.org/licenses/odbl/)


## Reproducible Computational Environments (Docker)

To share the computing environment a container image ([online](https://hub.docker.com/r/nehemie/copernicus))
and its associated Dockerfile [docker-image-archive.Dockerfile](docker-image-archive.Dockerfile) are available.

The script below use the file locally to build the container.

## Mapping Karabel

    # Note: all the following command must be used with bash within this folder

### Requirement

[An Earthdata Login](https://urs.earthdata.nasa.gov/) is required
before users can download NASA's Earth Observing System Data.
Credentials must be added in the file data/izmir_karabel/secret.yaml

To update the credentials, create the variables $yourlogin and
$yourpw or replace them with the commande below

    sed -i 's/mylogin/$yourlogin/g' izmir_karabel/secret.yaml
    sed -i 's/mypw/$yourpw/g' izmir_karabel/secret.yaml

### Reproducing the Computational Environment

    # Build and tag the container from local file

    docker build -t karabel:repro -f docker-image-archive.Dockerfile .

### Reproducing the map

The [Makefile](izmir_karabel/Makefile) in the folder
[izmir_karabel](izmir_karabel) can be executed to reproduce the map. You
can use the [container] to reproduce it

    # Run the container and mount this folder to execute

    docker container run --mount \
      type=bind,source=${PWD},target=/temp karabel:repro \
      bash -c "cd /temp/izmir_karabel/ && make -f Makefile"


You should find a file "Situation_Map_%Y-%m-%d.png" in the folder "./izmir_karabel/"

### Typical error

The download of OSM data may fail (if the server is overloaded).


> I wish you a nice journey!

           ★                           ★              ★               >
                        ★                      ★         ★            >
                                                                      >
         ★       ___                     ★                            >
                |   |                                                 >
                |   |                                                 >
               -------------------------------------                  >
    ~~~~~~~~~~ \ °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°° / ~~~~~~~~~~~~~    >
                \                  Data cargo     /      >=>          >
            >=>  \                               /                    >
     >=>          -------------------------------             >=>     >

